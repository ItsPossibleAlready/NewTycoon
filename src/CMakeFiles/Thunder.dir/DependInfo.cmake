# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/user/Projects/NewTycoon/src/Application.c" "/home/user/Projects/NewTycoon/src/CMakeFiles/Thunder.dir/Application.c.o"
  "/home/user/Projects/NewTycoon/src/EditArea.c" "/home/user/Projects/NewTycoon/src/CMakeFiles/Thunder.dir/EditArea.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "GETTEXT_PACKAGE=\"Thunder\""
  "I_KNOW_THE_PACKAGEKIT_GLIB2_API_IS_SUBJECT_TO_CHANGE"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "src"
  )

# Pairs of files generated by the same build rule.
set(CMAKE_MULTIPLE_OUTPUT_PAIRS
  "/home/user/Projects/NewTycoon/src/EditArea.c" "/home/user/Projects/NewTycoon/src/Application.c"
  )


# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
