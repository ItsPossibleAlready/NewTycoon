/*
* Copyright (c) 2011-2018 Your Organization (https://yourwebsite.com)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*
* Authored by: Author <author@example.com>
*/
// modules: gtk+-3.0
namespace Thunder {

    public class Notebook : Gtk.Notebook {

        public void newFile () {
            var newFileBuffer = new Gtk.TextBuffer (null);
            var newFileView = new Gtk.TextView.with_buffer (newFileBuffer);
            newFileView.set_wrap_mode (Gtk.WrapMode.WORD);

            var newFileScrolledWindow = new Gtk.ScrolledWindow (null ,null);
            newFileScrolledWindow.set_policy (Gtk.PolicyType.AUTOMATIC,
                                              Gtk.PolicyType.AUTOMATIC);

            newFileScrolledWindow.add (newFileView);
            newFileScrolledWindow.set_border_width (5);

            var newFileTitle = new Gtk.Label ("Untitled");

            this.append_page ( newFileScrolledWindow , newFileTitle);
        }

        public void openFile(){
            string k = "Hello World";
            var openFileTextTag = new Gtk.TextTag (k);
            var openFileTextTagTable = new Gtk.TextTagTable ();
            openFileTextTagTable.add (openFileTextTag);
            var openFileBuffer = new Gtk.TextBuffer (openFileTextTagTable);
            var openFileView = new Gtk.TextView.with_buffer (openFileBuffer);
            openFileView.set_wrap_mode (Gtk.WrapMode.WORD);

            var openFileScrolledWindow = new Gtk.ScrolledWindow (null ,null);
            openFileScrolledWindow.set_policy (Gtk.PolicyType.AUTOMATIC,
                                               Gtk.PolicyType.AUTOMATIC);

            openFileScrolledWindow.add (openFileView);
            openFileScrolledWindow.set_border_width (5);

            var openFileTitle = new Gtk.Label ("Hello.vala");

            this.append_page (openFileScrolledWindow , openFileTitle);
        }
        public Notebook (){
            this.openFile();
            this.newFile();
        }
    }
}
