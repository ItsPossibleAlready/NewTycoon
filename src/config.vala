namespace Eddy.Constants {
    public const string DATADIR = "/usr/local/share";
    public const string PKGDATADIR = "/usr/local/share/Thunder";
    public const string GETTEXT_PACKAGE = "Thunder";
    public const string RELEASE_NAME = "";
    public const string VERSION = "1.0.0";
    public const string VERSION_INFO = "Unstable";
}
