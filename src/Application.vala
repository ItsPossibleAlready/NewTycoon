/*
* Copyright (c) 2011-2018 Your Organization (https://yourwebsite.com)
*
* This program is free software; you can redistribute it and/or
* modify it under the terms of the GNU General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this program; if not, write to the
* Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
* Boston, MA 02110-1301 USA
*
* Authored by: Author <author@example.com>
*/
// modules: gkt+-3.0
namespace Thunder {

public class Application : Gtk.Application {
    public Application () {
        Object (
            application_id: "com.github.ItsPossibleAlready.NewTycoon",
            flags: ApplicationFlags.FLAGS_NONE
        );
    }


    protected override void activate () {
        var editArea = new Notebook ();

        var Header = new Gtk.HeaderBar ();
        Header = new Gtk.HeaderBar ();
        Header.set_title ("ThunderIDE");
        Header.set_subtitle ("To Develop faster than light");
        Header.show_close_button = true;

        var nightModeSwitch = new Gtk.Switch ();
        nightModeSwitch.notify["active"].connect(()=> {
        });
        var newDocumentButton = new Gtk.Button.from_icon_name ("document-new", Gtk.IconSize.MENU);
        Header.pack_start(newDocumentButton);
        Header.pack_start(nightModeSwitch);
        newDocumentButton.clicked.connect(() => {
            editArea.newFile();
            });
        var main_window = new Gtk.ApplicationWindow(this);
        main_window.default_height = 500;
        main_window.default_width = 1200;
        main_window.title = "Thunder";
        main_window.add(editArea);
        main_window.set_titlebar(Header);
        main_window.show_all ();
    }

    public static int main (string[] args) {
        var app = new Application ();
        return app.run (args);
    }
}

}
